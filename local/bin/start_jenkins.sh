#!/bin/sh

set -e

exec su-exec ${RUN_USER_NAME:-forumi0721} java -DJENKINS_HOME=${JENKINS_HOME:-/repo} -jar /app/jenkins/jenkins.war


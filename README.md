# alpine-jenkins
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-jenkins)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-jenkins)



----------------------------------------
### x64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-jenkins/x64)
### aarch64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/alpine-jenkins/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [Jenkins](https://jenkins.io/)
    - Jenkins is an automation engine with an unparalleled plugin ecosystem to support all of your favorite tools in your delivery pipelines, whether your goal is continuous integration, automated testing, or continuous delivery. 



----------------------------------------
#### Run

```sh
docker run -d \
           -p 8080:8080/tcp \
           -v repo:/repo \
           forumi0721/alpine-jenkins:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [http://localhost:8080/](http://localhost:8080/)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 8080/tcp           | HTTP port                                        |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| /repo              | Persistent data                                  |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

